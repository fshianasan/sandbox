// ==UserScript==
// @author      fshianasan
// @name        nicolive_lite
// @namespace   http://fshianasan.net
// @description ニコニコ生放送のページを軽くしたかった。
// @include     http://live.nicovideo.jp/watch/lv*
// ==/UserScript==

(function() {
  // 放送画面下の放送一覧
  var zappingBox = document.getElementById('watch_zapping_box');
  zapppingBox.style.display = 'none';
  
  // 放送一覧下のテキスト一覧
  var watchBottomBox = document.getElementById('watch_player_bottom_box');
  watchBottomBox.style.display = 'none';
})();
