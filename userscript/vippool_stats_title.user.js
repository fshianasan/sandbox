// ==UserScript==
// @name        vippool_stats_title
// @namespace   http://fshianasan.net/
// @description VIP Poolのダッシュボードの My Hashrate, My Sharerate をウィンドウタイトルに表示させます。
// @include     https://vippool.net/index.php?page=dashboard
// ==/UserScript==

(function() {
  setInterval(function() {
    var hash = document.getElementById('b-hashrate').innerHTML;
    var share = document.getElementById('b-sharerate').innerHTML;
    document.title = "H:" + hash + "KH/s, S:" + share + "";
  }, 1000 * 5)
})();
