s = #("渚カヲル")
print(s)

s = string.sub("渚カヲル", 2, 3)
print(s)

s = string.find("渚カヲル", "渚")
print(s)

s = string.gsub("渚カヲル", "カヲル", "オワリ")
print(s)

s = string.upper("kaworu nagisa")
print(s)

s = string.lower("KAWORU NAGISA")
print(s)

s = string.reverse("渚カヲル")
print(s)
