cm = arg[1]
print "あなたの身長は？"

function convert(cm)
  t = {
    "inch\t" .. cm / 2.54,
    "feet\t" .. cm / 30.48,
    "yard\t" .. cm / 91.44,
    "digit\t" .. cm / 1.905,
    "mile\t" .. cm / 160934.4,
    "hyde\t" .. cm / 156,
    "furlong\t" ..  cm / 20116.8402,
    "寸\t" .. cm / 3.0303,
    "尺\t" .. cm / 30.303,
    "里\t" .. cm / 392727,
    "海里\t" .. cm / 185200
  }

  for i, v in ipairs(t) do
    print(v)
  end
end

convert(cm)
